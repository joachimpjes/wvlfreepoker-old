<?
session_start();

//require the config file
require("config.php");
require("functions.php");
//make the connection to the database
$connection = @mysql_connect($server, $dbusername, $dbpassword) or die(mysql_error());
$db = @mysql_select_db($db_name,$connection)or die(mysql_error());

?>
<!DOCTYPE html>
<html lang="en">

<head>
	<!-- Title -->
	<title>WVLFreepoker</title>
	<link rel="shortcut icon" type="image/x-icon" href="/favicon.ico">
    <link rel="stylesheet" type="text/css" href="css/grid.css"/>
    <link rel="stylesheet" type="text/css" href="css/tabel.css"/>
    <link rel="stylesheet" type="text/css" href="css/style.css"/>
    <link href="css/responsive-nav.css" rel="stylesheet" type="text/css">
     <meta name="viewport" content="width=device-width, initial-scale=1.0">
	<!-- Scripts -->
	<script src="js/responsive-nav.js"></script>
    <script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-52301194-1', 'auto');
  ga('send', 'pageview');

</script>
</head>

<?
error_reporting(E_ALL & ~E_NOTICE);
?>
<body>
<div class="container_12">

	<div class="header">
	<div class="grid_4"><a href="index.php"><img src="images/logo.png" alt="" /></a></div>
	<div class="grid_8">					
    <nav class="nav-collapse">
    <ul>
	<li><a href="index.php">Home</a></li>
	<li>Kalender
    <ul class="submenu">
<li><a href="kalender.php">WVLFreepoker</a></li>
<li><a href="foolhouse.php">Foolhouse</a></li>
</ul></li>	
<li>Klassement
<ul class="submenu">
<li><a href="klassement.php">Algemeen</a></li>
<li><a href="klassement.php?id=7&d=3">Biljart Palace</a></li>
<li><a href="klassement.php?id=14">De Vetten Os</a></li>
<li><a href="klassement.php?id=11">Evans</a></li>
<li><a href="klassement.php?id=4">Smoking Cue</a></li>
<li><a href="klassement.php?id=13">Krekel</a></li>
<li><a href="klassement.php?id=3">El Dia</a></li>
</ul>
    </li>
    <li><a href="reglement.php">Reglement</a></li>
    <li><a href="contact.php">Over Ons</a></li>
    <?
	if(!isset($_SESSION['user_name'])){
	?>
    <li><a href="registreren.php">Registreren/Inloggen</a></li>
    <?
	}else{
	?>
    <li><a href="logout.php">Uitloggen</a></li>
    <?
	if(allow_access("Administrators") == "yes"){
	?>
    <li><a href="admin/">Admin</a></li>
    <?
	}
	}
	?>
</ul></nav>
</div>
</div>
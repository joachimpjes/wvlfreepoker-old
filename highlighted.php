<div class="top">
<div class="grid_3"><img src="images/wvl-logo-small.png" alt="freeroll-icoon" class="align-center">
  <h2><a href="kalender.php">Freepoker Events</a></h2>
<p>Live en Gratis! Sfeer en gezelligheid ten troef! Kijk snel waar er een toernooi plaatsvind!</p>
</div>
<div class="grid_3"><img src="images/foolhouse-icon.png" alt="Foolhouse-Icoon" class="align-center">
  <h2><a href="foolhouse.php">Foolhouse</a></h2>
<p>Onze pokerclub uit Ingelmunster die 2 wekelijks plaatsvind! Meer info kunt u hier terugvinden</p>
</div>
<div class="grid_3"><img src="images/special-icon.png" alt="deepstack-icoon" class="align-center">
  <h2><a href="#">Specials</a></h2>
<p>Qualifiers, Buyins, Steps,... Allerlei events voor verschillende prijzen en buyins</p>
</div>
<div class="grid_3"><img src="images/ranking-icon.png" alt="ranking-icoon" class="align-center">
  <h2><a href="klassement.php">Ranking</a></h2>
<p>Sta je op nummer 1? Bekijk het hier snel! Zowel Algemeen, All-Time als per locatie. </p>
</div>
</div>
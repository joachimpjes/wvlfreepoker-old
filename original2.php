<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" dir="ltr" lang="en-US">

<head>

	<!-- Meta tags -->
	<meta http-equiv="content-type" content="text/html; charset=UTF-8" />

	<!-- Title -->
	<title>Design | Websource WP</title>
	
	<link rel="profile" href="http://gmpg.org/xfn/11" />
	<link rel="stylesheet" type="text/css" media="all" href="http://websource.freepsdtheme.com/wp-content/themes/websource/style.css" />
	<link rel="pingback" href="http://websource.freepsdtheme.com/xmlrpc.php" />
	
	<meta name='robots' content='noindex,nofollow' />
<link rel="alternate" type="application/rss+xml" title="Websource WP &raquo; Feed" href="http://websource.freepsdtheme.com/feed/" />
<link rel="alternate" type="application/rss+xml" title="Websource WP &raquo; Comments Feed" href="http://websource.freepsdtheme.com/comments/feed/" />
<link rel="alternate" type="application/rss+xml" title="Websource WP &raquo; Design Category Feed" href="http://websource.freepsdtheme.com/category/design/feed/" />
<link rel='stylesheet' id='wp-pagenavi-css'  href='http://websource.freepsdtheme.com/wp-content/plugins/wp-pagenavi/pagenavi-css.css?ver=2.70' type='text/css' media='all' />
<script type='text/javascript' src='http://websource.freepsdtheme.com/wp-includes/js/jquery/jquery.js?ver=1.4.2'></script>
<link rel="EditURI" type="application/rsd+xml" title="RSD" href="http://websource.freepsdtheme.com/xmlrpc.php?rsd" />
<link rel="wlwmanifest" type="application/wlwmanifest+xml" href="http://websource.freepsdtheme.com/wp-includes/wlwmanifest.xml" /> 
<link rel='index' title='Websource WP' href='http://websource.freepsdtheme.com/' />
<meta name="generator" content="WordPress 3.0.1" />
	
	<!-- Scripts -->
	
		
	<script type="text/javascript">
	var theInt = null;
		var $crosslink, $navthumb;
		var curclicked = 0;
		
		theInterval = function(cur){
			clearInterval(theInt);
			
			if( typeof cur != 'undefined' )
				curclicked = cur;
			
			$crosslink.removeClass("active-thumb");
			$navthumb.eq(curclicked).parent().addClass("active-thumb");
				var title = $navthumb.eq(curclicked).parent().attr('title');
				$j('#featured h3').html(title);
				$j(".stripNav ul li a").eq(curclicked).trigger('click');
			
			theInt = setInterval(function(){
				$crosslink.removeClass("active-thumb");
				$navthumb.eq(curclicked).parent().addClass("active-thumb");
				$j(".stripNav ul li a").eq(curclicked).trigger('click');
				var title = $navthumb.eq(curclicked).parent().attr('title');
				$j('#featured h3').html(title);
				curclicked++;
				if( 10 == curclicked )
					curclicked = 0;
				
			}, 3000);
		};
	</script>			<meta name='robots' content='noindex,nofollow' />
<link rel="alternate" type="application/rss+xml" title="Websource WP &raquo; Feed" href="http://websource.freepsdtheme.com/feed/" />
<link rel="alternate" type="application/rss+xml" title="Websource WP &raquo; Comments Feed" href="http://websource.freepsdtheme.com/comments/feed/" />
<link rel="alternate" type="application/rss+xml" title="Websource WP &raquo; Design Category Feed" href="http://websource.freepsdtheme.com/category/design/feed/" />
<link rel="EditURI" type="application/rsd+xml" title="RSD" href="http://websource.freepsdtheme.com/xmlrpc.php?rsd" />
<link rel="wlwmanifest" type="application/wlwmanifest+xml" href="http://websource.freepsdtheme.com/wp-includes/wlwmanifest.xml" /> 
<link rel='index' title='Websource WP' href='http://websource.freepsdtheme.com/' />
<meta name="generator" content="WordPress 3.0.1" />
		
</head>

<body class="archive category category-design">

<div id="container">

	<div id="header" class="group">
	
		<div id="nav">
							<ul id="menu-top-menu" class="pages group"><li id="menu-item-35" class="menu-item menu-item-type-custom menu-item-home menu-item-35"><a href="http://websource.freepsdtheme.com">Home</a></li>
<li id="menu-item-4" class="menu-item menu-item-type-post_type menu-item-4"><a href="http://websource.freepsdtheme.com/about/">About</a></li>
<li id="menu-item-295" class="menu-item menu-item-type-custom menu-item-295"><a href="#">Dropdown</a>
<ul class="sub-menu">
	<li id="menu-item-296" class="menu-item menu-item-type-post_type menu-item-296"><a href="http://websource.freepsdtheme.com/advertise/">Advertise</a></li>
	<li id="menu-item-297" class="menu-item menu-item-type-post_type menu-item-297"><a href="http://websource.freepsdtheme.com/contact-us/">Contact us</a></li>
	<li id="menu-item-298" class="menu-item menu-item-type-post_type menu-item-298"><a href="http://websource.freepsdtheme.com/about/">About</a></li>
</ul>
</li>
<li id="menu-item-303" class="menu-item menu-item-type-custom menu-item-303"><a href="http://themeforest.net/item/websource-wordpress-magazine-theme/111981">Buy this theme</a></li>
</ul>				
							<ul id="menu-secondarymenu" class="categories group"><li id="menu-item-256" class="menu-item menu-item-type-taxonomy menu-item-256"><a href="http://websource.freepsdtheme.com/category/featured/">Featured</a></li>
<li id="menu-item-25" class="menu-item menu-item-type-taxonomy current-menu-item current-category-ancestor menu-item-25"><a href="http://websource.freepsdtheme.com/category/design/">Design</a></li>
<li id="menu-item-26" class="menu-item menu-item-type-taxonomy menu-item-26"><a href="http://websource.freepsdtheme.com/category/technology/">Technology</a></li>
<li id="menu-item-33" class="menu-item menu-item-type-taxonomy menu-item-33"><a href="http://websource.freepsdtheme.com/category/sports/">Sports</a></li>
<li id="menu-item-270" class="menu-item menu-item-type-custom current-menu-ancestor current-menu-parent menu-item-270"><a href="#">Categories</a>
<ul class="sub-menu">
	<li id="menu-item-271" class="menu-item menu-item-type-taxonomy menu-item-271"><a href="http://websource.freepsdtheme.com/category/featured/">Featured</a></li>
	<li id="menu-item-272" class="menu-item menu-item-type-taxonomy current-menu-item current-category-ancestor menu-item-272"><a href="http://websource.freepsdtheme.com/category/design/">Design</a></li>
	<li id="menu-item-273" class="menu-item menu-item-type-taxonomy menu-item-273"><a href="http://websource.freepsdtheme.com/category/technology/">Technology</a></li>
	<li id="menu-item-274" class="menu-item menu-item-type-taxonomy menu-item-274"><a href="http://websource.freepsdtheme.com/category/sports/">Sports</a></li>
</ul>
</li>
</ul>					</div><!-- /nav -->
		
		
						<h1 class="logo"><a href="http://websource.freepsdtheme.com/"><img src="http://websource.freepsdtheme.com/wp-content/uploads/2010/07/logoupload.png" alt="" /></a></h1>
				
		
								<div class="ad"><a href="#"><img src="http://websource.freepsdtheme.com/wp-content/uploads/2010/07/bigad1.jpg" alt="WooThemes" /></a></div><!-- /ad -->
			
	</div><!-- /header -->
<div id="content" class="group">

<div id="main">
	
		<div class="latest group">
		
						
			<h3>
			Design			</h3>
			
						
						
			<div class="post">
				<a href="http://websource.freepsdtheme.com/2010/07/character-illustration-creative-session/"><img width="259" height="76" src="http://websource.freepsdtheme.com/wp-content/uploads/2010/07/character-259x76.jpg" class="attachment-medium wp-post-image" alt="character" title="character" /></a>
				<h4><a href="http://websource.freepsdtheme.com/2010/07/character-illustration-creative-session/">Character Illustration / Creative Session</a></h4>
				<p class="date">July 3rd 2010</p>
				<p><p>Character illustration is a huge creative field. It is intimately connected to animation and comic traditions, and has a long history of being used in advertising to both promote products and brand companies. Creative Session 1 – Project Brief Sessions are blocks of articles, interviews, tutorials and content on a particular creative subject. Sessions are [...]</p>
</p>
			</div>
			
						
			<div class="post">
				<a href="http://websource.freepsdtheme.com/2010/07/adobe-creative-suite-5/"><img width="259" height="76" src="http://websource.freepsdtheme.com/wp-content/uploads/2010/07/adobe-259x76.jpg" class="attachment-medium wp-post-image" alt="adobe" title="adobe" /></a>
				<h4><a href="http://websource.freepsdtheme.com/2010/07/adobe-creative-suite-5/">Adobe Creative Suite 5</a></h4>
				<p class="date">July 3rd 2010</p>
				<p><p>Adobe has always been dedicated to helping you bring your creative ideas to life. Today we&#8217;re all experiencing an unprecedented amount of change on the web, in publishing, and across all forms of media and advertising. Start here. Go anywhere. Discover breakthrough interactive design tools that enable you to create, deliver, and optimize beautiful, high-impact [...]</p>
</p>
			</div>
			
						
			<div class="post">
				<a href="http://websource.freepsdtheme.com/2010/07/video-test/"><img width="259" height="76" src="http://websource.freepsdtheme.com/wp-content/uploads/2010/07/milliondollar1-259x76.jpg" class="attachment-medium wp-post-image" alt="milliondollar" title="milliondollar" /></a>
				<h4><a href="http://websource.freepsdtheme.com/2010/07/video-test/">1MD Showreel 2010 video</a></h4>
				<p class="date">July 3rd 2010</p>
				<p><p>MD, aka One Million Dollars, is the name of a creative agency, located in Belgium and founded on the 1st April 2009. About One Million Dollars We are passionate about interactive campaigns, we treat social networks as they should be treated, we respect the web and its users, and we want all our projects to [...]</p>
</p>
			</div>
			
						
			<div class="post">
				<a href="http://websource.freepsdtheme.com/2010/07/digital-illustration-creative-session/"><img width="259" height="76" src="http://websource.freepsdtheme.com/wp-content/uploads/2010/07/di-259x76.jpg" class="attachment-medium wp-post-image" alt="di" title="di" /></a>
				<h4><a href="http://websource.freepsdtheme.com/2010/07/digital-illustration-creative-session/">Digital Illustration / Creative Session</a></h4>
				<p class="date">July 1st 2010</p>
				<p><p>Character illustration is a huge creative field. It is intimately connected to animation and comic traditions, and has a long history of being used in advertising to both promote products and brand companies. Creative Session 1 – Project Brief Sessions are blocks of articles, interviews, tutorials and content on a particular creative subject. Sessions are [...]</p>
</p>
			</div>
			
								
		</div><!-- /latest -->
		
		<div class="pagination"></div>		

	</div><!-- /main -->


<div id="sidebar">

	<div class="search widget">
	
		<form role="search" method="get" id="searchform" action="http://websource.freepsdtheme.com/" >
	<div><label class="screen-reader-text" for="s">Search for:</label>
	<input type="text" value="" name="s" id="s" />
	<input type="submit" id="searchsubmit" value="Search" />
	</div>
	</form>	
	</div><!-- /search -->
	
	<div class="social widget">
		
						<a href="http://feeds.feedburner.com/freepsdtheme" class="rss">770<span>Subscribers</span></a>
				
						<a href="http://twitter.com/ddstudios" class="twitter"><br />
<b>Fatal error</b>:  Call to a member function removeChild() on a non-object in <b>/nfs/c06/h04/mnt/92973/domains/websource.freepsdtheme.com/html/wp-content/themes/websource/functions/theme_counters.php</b> on line <b>20</b><br />

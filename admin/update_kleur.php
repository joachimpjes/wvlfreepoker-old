<?php

//prevents caching
header("Expires: Sat, 01 Jan 2000 00:00:00 GMT");
header("Last-Modified: ".gmdate("D, d M Y H:i:s")." GMT");
header("Cache-Control: post-check=0, pre-check=0",false);
session_cache_limiter();
session_start();

require('../config.php');
require('../functions.php');

//check for administrative rights
if (allow_access("Administrators") != "yes")
{
	include ('../no_access.html');
	exit;
}

//make connection to dbase
$connection = @mysql_connect($server, $dbusername, $dbpassword)
			or die(mysql_error());
			
$db = @mysql_select_db($db_name,$connection)
			or die(mysql_error());

//build and issue the query
$sql ="SELECT * FROM $table_name";
$result = @mysql_query($sql,$connection) or die(mysql_error());


$maand='12';
$jaar="2014";
$maandstart = $maand-1;
$maandeind = $maand+1;
$vpunten = 0;
if($maandeind == '13'){
	$maandeind = 1;
	$jaareind = $jaar+1;
}else{
	$jaareind = $jaar;
}


$sql = "UPDATE klassement SET kleur='' WHERE maand='".$maand."' AND jaar='".$jaar."' AND locatie='1' AND dag='1'";
$resultaat = mysql_query($sql) or die (mysql_error());
$sql = "SELECT aantal FROM kwalificatie WHERE maand='".$maand."' AND jaar='".$jaar."' AND locatie='1' AND dag='1'";
$resultaat = mysql_query($sql) or die (mysql_error());
$num = mysql_num_rows($resultaat);
if($num == '0'){
	$sql = "SELECT username, punten FROM klassement WHERE maand='".$maand."' AND jaar='".$jaar."' AND locatie='1' AND dag='1'";
	$resultaat = mysql_query($sql) or die (mysql_error());
	$num = mysql_num_rows($resultaat);
	$num2 = ceil($num/5);
}else{
	$row = mysql_fetch_assoc($resultaat);
	$num2 = $row['aantal'];
}
$sql = "UPDATE klassement SET kleur='r' WHERE maand='".$maand."' AND jaar='".$jaar."' AND locatie='1' AND dag='1' AND rank<='".$num2."'";
$resultaat = mysql_query($sql) or die (mysql_error());

$sql = "UPDATE klassement SET kleur='' WHERE maand='".$maand."' AND jaar='".$jaar."' AND locatie='1' AND dag='4'";
$resultaat = mysql_query($sql) or die (mysql_error());
$sql = "SELECT aantal FROM kwalificatie WHERE maand='".$maand."' AND jaar='".$jaar."' AND locatie='1' AND dag='4'";
$resultaat = mysql_query($sql) or die (mysql_error());
$num = mysql_num_rows($resultaat);
if($num == '0'){
	$sql = "SELECT username, punten FROM klassement WHERE maand='".$maand."' AND jaar='".$jaar."' AND locatie='1' AND dag='4'";
	$resultaat = mysql_query($sql) or die (mysql_error());
	$num = mysql_num_rows($resultaat);
	$num2 = round($num/5);
}else{
	$row = mysql_fetch_assoc($resultaat);
	$num2 = $row['aantal'];
}
$sql = "UPDATE klassement SET kleur='r' WHERE maand='".$maand."' AND jaar='".$jaar."' AND locatie='1' AND dag='4' AND rank<='".$num2."'";
$resultaat = mysql_query($sql) or die (mysql_error());

$sql = "UPDATE klassement SET kleur='' WHERE maand='".$maand."' AND jaar='".$jaar."' AND locatie='7' AND dag='3'";
$resultaat = mysql_query($sql) or die (mysql_error());
$sql = "SELECT aantal FROM kwalificatie WHERE maand='".$maand."' AND jaar='".$jaar."' AND locatie='7' AND dag='3'";
$resultaat = mysql_query($sql) or die (mysql_error());
$num = mysql_num_rows($resultaat);
if($num == '0'){
	$sql = "SELECT username, punten FROM klassement WHERE maand='".$maand."' AND jaar='".$jaar."' AND locatie='7' AND dag='3'";
	$resultaat = mysql_query($sql) or die (mysql_error());
	$num = mysql_num_rows($resultaat);
	$num2 = ceil($num/5);
}else{
	$row = mysql_fetch_assoc($resultaat);
	$num2 = $row['aantal'];
}
$sql = "UPDATE klassement SET kleur='r' WHERE maand='".$maand."' AND jaar='".$jaar."' AND locatie='7' AND dag='3' AND rank<='".$num2."'";
$resultaat = mysql_query($sql) or die (mysql_error());

$sql = "UPDATE klassement SET kleur='' WHERE maand='".$maand."' AND jaar='".$jaar."' AND locatie='7' AND dag='5'";
$resultaat = mysql_query($sql) or die (mysql_error());
$sql = "SELECT aantal FROM kwalificatie WHERE maand='".$maand."' AND jaar='".$jaar."' AND locatie='7' AND dag='5'";
$resultaat = mysql_query($sql) or die (mysql_error());
$num = mysql_num_rows($resultaat);
if($num == '0'){
	$sql = "SELECT username, punten FROM klassement WHERE maand='".$maand."' AND jaar='".$jaar."' AND locatie='7' AND dag='5'";
	$resultaat = mysql_query($sql) or die (mysql_error());
	$num = mysql_num_rows($resultaat);
	$num2 = ceil($num/7);
}else{
	$row = mysql_fetch_assoc($resultaat);
	$num2 = $row['aantal'];
}
$sql = "UPDATE klassement SET kleur='r' WHERE maand='".$maand."' AND jaar='".$jaar."' AND locatie='7' AND dag='5' AND rank<='".$num2."'";
$resultaat = mysql_query($sql) or die (mysql_error());

$activelocaties = array(2,3,4,8,9,10,11,12,13,14);

foreach ($activelocaties as &$locatie) {

$sql = "UPDATE klassement SET kleur='' WHERE maand='".$maand."' AND jaar='".$jaar."' AND locatie='".$locatie."' AND dag='0'";
$resultaat = mysql_query($sql) or die (mysql_error());
$sql = "SELECT aantal FROM kwalificatie WHERE maand='".$maand."' AND jaar='".$jaar."' AND locatie='".$locatie."' AND dag='0'";
$resultaat = mysql_query($sql) or die (mysql_error());
$num = mysql_num_rows($resultaat);
if($num == '0'){
	$sql = "SELECT username, punten FROM klassement WHERE maand='".$maand."' AND jaar='".$jaar."' AND locatie='".$locatie."' AND dag='0'";
	$resultaat = mysql_query($sql) or die (mysql_error());
	$num = mysql_num_rows($resultaat);
	$num2 = ceil($num/5);
}else{
	$row = mysql_fetch_assoc($resultaat);
	$num2 = $row['aantal'];
}
$sql = "UPDATE klassement SET kleur='r' WHERE maand='".$maand."' AND jaar='".$jaar."' AND locatie='".$locatie."' AND dag='0' AND rank<='".$num2."'";
$resultaat = mysql_query($sql) or die (mysql_error());
}

for($j=1;$j<4;$j++){
$kwal1 = array();
$kwal2 = array();
$sql = "SELECT username FROM kwalificatieonline WHERE maand='".$maand."' AND jaar='".$jaar."'";
$resultaat = mysql_query($sql) or die (mysql_error());
while($row = mysql_fetch_assoc($resultaat)){
	$sql2 = "SELECT rank, username, punten, locatie, dag FROM klassement WHERE username='".$row['username']."' AND maand='".$maand."' AND jaar='".$jaar."' AND kleur='r' AND locatie<>'0' OR username='".$row['username']."' AND maand='".$maand."' AND jaar='".$jaar."' AND kleur='o' AND locatie<>'0' ORDER BY rank ASC, punten DESC";
	$resultaat2 = mysql_query($sql2) or die (mysql_error());
	while($row2 = mysql_fetch_assoc($resultaat2)){
		$sql4 = "SELECT rank, username, punten, locatie, dag FROM klassement WHERE maand='".$maand."' AND jaar='".$jaar."' AND kleur='' AND locatie='".$row2['locatie']."' AND dag='".$row2['dag']."' ORDER BY rank ASC LIMIT 1";
		$resultaat4 = mysql_query($sql4) or die (mysql_error());
		$row4 = mysql_fetch_assoc($resultaat4);
		$sql5 = "UPDATE klassement SET kleur='o' WHERE maand='".$maand."' AND jaar='".$jaar."' AND locatie='".$row2['locatie']."' AND dag='".$row2['dag']."' AND username='".$row4['username']."'";
		$resultaat5 = mysql_query($sql5) or die (mysql_error());
	}
	$sql3 = "UPDATE klassement SET kleur='p' WHERE maand='".$maand."' AND jaar='".$jaar."' AND username='".$row['username']."'";
	$resultaat3 = mysql_query($sql3) or die (mysql_error());
	array_push($kwal1,$row['username']);
}

$sql = "SELECT username FROM kwalificatiewildcard WHERE maand='".$maand."' AND jaar='".$jaar."'";
$resultaat = mysql_query($sql) or die (mysql_error());
while($row = mysql_fetch_assoc($resultaat)){
	$sql2 = "SELECT rank, username, punten, locatie, dag FROM klassement WHERE username='".$row['username']."' AND maand='".$maand."' AND jaar='".$jaar."' AND kleur='r' AND locatie<>'0' OR username='".$row['username']."' AND maand='".$maand."' AND jaar='".$jaar."' AND kleur='o' AND locatie<>'0' ORDER BY rank ASC, punten DESC";
	$resultaat2 = mysql_query($sql2) or die (mysql_error());
	while($row2 = mysql_fetch_assoc($resultaat2)){
		$sql4 = "SELECT rank, username, punten, locatie, dag FROM klassement WHERE maand='".$maand."' AND jaar='".$jaar."' AND kleur='' AND locatie='".$row2['locatie']."' AND dag='".$row2['dag']."' ORDER BY rank ASC LIMIT 1";
		$resultaat4 = mysql_query($sql4) or die (mysql_error());
		$row4 = mysql_fetch_assoc($resultaat4);
		$sql5 = "UPDATE klassement SET kleur='o' WHERE maand='".$maand."' AND jaar='".$jaar."' AND locatie='".$row2['locatie']."' AND dag='".$row2['dag']."' AND username='".$row4['username']."'";
		$resultaat5 = mysql_query($sql5) or die (mysql_error());
	}
	$sql3 = "UPDATE klassement SET kleur='w' WHERE maand='".$maand."' AND jaar='".$jaar."' AND username='".$row['username']."'";
	$resultaat3 = mysql_query($sql3) or die (mysql_error());
	$sql3 = "SELECT kleur FROM klassement WHERE maand='".$maand."' AND jaar='".$jaar."' AND username='".$row['username']."'";
	$resultaat3 = mysql_query($sql3) or die (mysql_error());
	$row3 = mysql_fetch_assoc($resultaat3);
	array_push($kwal1,$row['username']);
}

$sql = "SELECT username FROM klassement WHERE maand='".$maand."' AND jaar='".$jaar."' AND kleur='r' OR maand='".$maand."' AND jaar='".$jaar."' AND kleur='o'";
$resultaat = mysql_query($sql) or die (mysql_error());
while($row = mysql_fetch_assoc($resultaat)){
	if(in_array($row['username'],$kwal1)){
		if(!in_array($row['username'],$kwal2)){
			array_push($kwal2,$row['username']);
		}
	}else{
		array_push($kwal1,$row['username']);
	}
}
$count = count($kwal2);
for($i=0;$i<$count;$i++){
	$sql2 = "SELECT rank, username, punten, locatie, dag FROM klassement WHERE username='".$kwal2[$i]."' AND maand='".$maand."' AND jaar='".$jaar."' AND kleur='r' AND locatie<>'0' OR username='".$kwal2[$i]."' AND maand='".$maand."' AND jaar='".$jaar."' AND kleur='o' AND locatie<>'0' ORDER BY rank ASC, punten DESC";
	$resultaat2 = mysql_query($sql2) or die (mysql_error());
	$row2 = mysql_fetch_assoc($resultaat2);
	while($row2 = mysql_fetch_assoc($resultaat2)){
		$sql3 = "UPDATE klassement SET kleur='a' WHERE maand='".$maand."' AND jaar='".$jaar."' AND locatie='".$row2['locatie']."' AND dag='".$row2['dag']."' AND username='".$kwal2[$i]."'";
		$resultaat3 = mysql_query($sql3) or die (mysql_error());
		$sql4 = "SELECT rank, username, punten, locatie, dag FROM klassement WHERE maand='".$maand."' AND jaar='".$jaar."' AND kleur='' AND locatie='".$row2['locatie']."' AND dag='".$row2['dag']."' ORDER BY rank ASC LIMIT 1";
		$resultaat4 = mysql_query($sql4) or die (mysql_error());
		$row4 = mysql_fetch_assoc($resultaat4);
		$sql5 = "UPDATE klassement SET kleur='o' WHERE maand='".$maand."' AND jaar='".$jaar."' AND locatie='".$row2['locatie']."' AND dag='".$row2['dag']."' AND username='".$row4['username']."'";
		$resultaat5 = mysql_query($sql5) or die (mysql_error());
		if(in_array($row4['username'],$kwal1)){
			array_push($kwal2,$row4['username']);
		}else{
			array_push($kwal1,$row4['username']);
		}
	}
	$count = count($kwal2);
}
}
#algemeen klassement
$sql2 = "DELETE FROM klassement WHERE maand='".$maand."' AND jaar='".$jaar."' AND locatie='0'";	
$resultaat2 = mysql_query($sql2) or die (mysql_error());
	$sql = "SELECT u.username, SUM(u.punten) AS punten FROM uitslagen AS u, kalender AS k WHERE k.datum<'".$jaareind."-".$maandeind."-01' AND k.datum>='".$jaar."-".$maandstart."-01' AND u.wid=k.id AND u.punten>0 GROUP BY u.username ORDER BY punten DESC";
$resultaat = mysql_query($sql) or die (mysql_error());
$i = 1;
while($row = mysql_fetch_assoc($resultaat)){
	if($vpunten ==  $row['punten']){
		$rank = $vrank;	
	}else{
		$rank =$i;	
	}
	$sql2 = "INSERT INTO klassement (maand,jaar,locatie,dag,username,rank,punten) VALUES('".$maand."','".$jaar."','0','0','".$row['username']."','".$rank."','".$row['punten']."')";	
	$resultaat2 = mysql_query($sql2) or die (mysql_error());
	$vpunten = $row['punten'];
	$vrank = $rank;
	$i++;
}
$sql = "SELECT username FROM kwalificatieonline WHERE jaar='".$jaar."' AND maand='".$maand."' AND username NOT IN (SELECT username FROM klassement WHERE jaar='".$jaar."' AND maand='".$maand."' and locatie='0')";
$resultaat = mysql_query($sql) or die (mysql_error());
while($row = mysql_fetch_assoc($resultaat)){
	if($vpunten ==  $row['punten']){
		$rank = $vrank;	
	}else{
		$rank =$i;	
	}
	$sql2 = "INSERT INTO klassement (maand,jaar,locatie,dag,username,rank,punten,kleur) VALUES('".$maand."','".$jaar."','0','0','".$row['username']."','".$rank."','0','p')";	
	$resultaat2 = mysql_query($sql2) or die (mysql_error());
	$vpunten = $row['punten'];
	$vrank = $rank;
	$i++;
}
$sql = "SELECT username FROM kwalificatiewildcard WHERE jaar='".$jaar."' AND maand='".$maand."' AND username NOT IN (SELECT username FROM klassement WHERE jaar='".$jaar."' AND maand='".$maand."' AND locatie='0')";
$resultaat = mysql_query($sql) or die (mysql_error());
while($row = mysql_fetch_assoc($resultaat)){
	if($vpunten ==  $row['punten']){
		$rank = $vrank;	
	}else{
		$rank =$i;	
	}
	$sql2 = "INSERT INTO klassement (maand,jaar,locatie,dag,username,rank,punten,kleur) VALUES('".$maand."','".$jaar."','0','0','".$row['username']."','".$rank."','0','w')";	
	$resultaat2 = mysql_query($sql2) or die (mysql_error());
	$vpunten = $row['punten'];
	$vrank = $rank;
	$i++;
}
$sql = "SELECT * FROM klassement WHERE maand='".$maand."' AND jaar='".$jaar."' AND kleur='r' OR maand='".$maand."' AND jaar='".$jaar."' AND kleur='o' OR maand='".$maand."' AND jaar='".$jaar."' AND kleur='p' OR maand='".$maand."' AND jaar='".$jaar."' AND kleur='w'";
$resultaat = mysql_query($sql) or die (mysql_error());
while($row = mysql_fetch_assoc($resultaat)){
	$sql2 = "UPDATE klassement SET kleur='".$row['kleur']."' WHERE username='".$row['username']."' AND maand='".$maand."' AND jaar='".$jaar."' AND locatie='0'";	
	$resultaat2 = mysql_query($sql2) or die (mysql_error());
}
?>
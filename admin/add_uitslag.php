<?php

//prevents caching
header("Expires: Sat, 01 Jan 2000 00:00:00 GMT");
header("Last-Modified: ".gmdate("D, d M Y H:i:s")." GMT");
header("Cache-Control: post-check=0, pre-check=0",false);
session_cache_limiter();
session_start();

require('../config.php');

require('../functions.php');

//this is group name or username of the group or person that you wish to allow access to
// - please be advise that the Administrators Groups has access to all pages.
if (allow_access("Administrators") != "yes")
{
include ('../no_access.html');
exit;
}
$connection = @mysql_connect($server, $dbusername, $dbpassword) or die(mysql_error());
$db = @mysql_select_db($db_name,$connection)or die(mysql_error());

$sql = "SELECT * FROM kalender WHERE datum>SUBDATE(NOW(),INTERVAL 7 MONTH) AND datum<NOW() ORDER BY datum DESC;";
$res = mysql_query($sql) or die (mysql_error());
echo '<ul>';
while ($wedstrijd = mysql_fetch_assoc($res)){
	$sql2 = "SELECT * FROM locaties WHERE id='".$wedstrijd['locatie']."'";
	$res2 = mysql_query($sql2) or die(mysql_error ());
	$row2 = mysql_fetch_array($res2);
	$wedstrijd_id = $wedstrijd['id'];
	$wedstrijd_naam = $wedstrijd['naam'];
	$wedstrijd_datum = $wedstrijd['datum'];
	$wedstrijd_locatie = $row2['naam'];
	$wedstrijd_plaats = $row2['plaats'];
	echo '<li>'.$wedstrijd_datum.' <a href="update_uitslag.php?id='.$wedstrijd_id.'">'.$wedstrijd_locatie.' ('.$wedstrijd_plaats.') '.$wedstrijd_naam.'</a></li>';
}
?>
</u>
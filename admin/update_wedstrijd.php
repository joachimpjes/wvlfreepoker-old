<?php

//prevents caching
header("Expires: Sat, 01 Jan 2000 00:00:00 GMT");
header("Last-Modified: ".gmdate("D, d M Y H:i:s")." GMT");
header("Cache-Control: post-check=0, pre-check=0",false);
session_cache_limiter();
session_start();

require('../config.php');

require('../functions.php');

//this is group name or username of the group or person that you wish to allow access to
// - please be advise that the Administrators Groups has access to all pages.
if (allow_access("Administrators") != "yes")
{
include ('../no_access.html');
exit;
}
$connection = @mysql_connect($server, $dbusername, $dbpassword) or die(mysql_error());
$db = @mysql_select_db($db_name,$connection)or die(mysql_error());

$sql = "SELECT * FROM kalender WHERE id = '".$_GET['id']."';";
$res = mysql_query($sql) or die (mysql_error());
while ($wedstrijd = mysql_fetch_assoc($res)){
$wedstrijd_locatie = $wedstrijd['locatie'];
$wedstrijd_datum = $wedstrijd['datum'];
$wedstrijd_tijd = $wedstrijd['tijd'];
$wedstrijd_special = $wedstrijd['special'];
$wedstrijd_finale = $wedstrijd['finale'];
$wedstrijd_foolhouse = $wedstrijd['foolhouse'];
$wedstrijd_wedstrijd = $wedstrijd['wedstrijd'];
$wedstrijd_foto = $wedstrijd['foto'];
$wedstrijd_beschrijving = $wedstrijd['beschrijving'];
}


if (isset($_POST['update_wedstrijd'])){

if($_POST['special'] != '1'){
	$_POST['special'] = '0';
}
if($_POST['finale'] != '1'){
	$_POST['finale'] = '0';
}
if($_POST['foolhouse'] != '1'){
	$_POST['foolhouse'] = '0';
}
$sql = "UPDATE kalender SET locatie = '".$_POST['locatie']."', datum = '".$_POST['datum']."', tijd = '".$_POST['tijd']."', 
special = '".$_POST['special']."', finale = '".$_POST['finale']."', foolhouse = '".$_POST['foolhouse']."', wedstrijd = '".$_POST['wedstrijd']."', foto = '".$_POST['foto']."', beschrijving = '".$_POST['beschrijving']."' WHERE id = '".$_GET['id']."';";$res = mysql_query($sql) or die (mysql_error());

echo 'De wedstrijd is succesvol geupdate';

} else {

?>

<form action="<?php echo $PHP_SELF;?>" method="post">

<table border="0" cellpadding="0" cellspacing="0" style="border-collapse: collapse" bordercolor="#111111" width="500">
  <tr>
   <td width="40%">Plaats</td>
   <td width="60%">
    <select size="1" name="locatie">
    <option value=""></option>

<?php
$sql = "SELECT * FROM locaties ORDER BY naam ASC;";
$resultaat = mysql_query($sql) or die (mysql_error());
while ($land = mysql_fetch_assoc($resultaat)){
if($land['id'] == $wedstrijd_locatie){
echo '    <option selected value="'.$land['id'].'">'.$land['naam'].'</option>';
} else {
echo '    <option value="'.$land['id'].'">'.$land['naam'].'</option>';
}
}
?>

    </select>
   </td>
  </tr>
  <tr>
   <td width="100%" height="8" colspan="2"></td>
  </tr>
  <tr>
   <td width="40%">Datum (jjjj-mm-dd)</td>
   <td width="60%"><input type="text" name="datum" value="<?php echo $wedstrijd_datum;?>" size="30"></td>
  </tr>
  <tr>
   <td width="100%" height="8" colspan="2"></td>
  </tr>
  <tr>
   <td width="40%">Tijd (uu:mm:ss)</td>
   <td width="60%"><input type="text" name="tijd" value="<?php echo $wedstrijd_tijd;?>" size="30"></td>
  </tr>
   <tr>
   <td width="100%" height="8" colspan="2"></td>
  </tr>
  <tr>
   <td width="40%">Special Event</td>
   <td width="60%"><input type="checkbox" name="special" value="1" <? if($wedstrijd_special == '1'){ echo 'checked';}?>></td>
  </tr>
  <tr>
   <td width="100%" height="8" colspan="2"></td>
  </tr>
  <tr>
   <td width="40%">Finale</td>
   <td width="60%"><input type="checkbox" name="finale" value="1" <? if($wedstrijd_finale == '1'){ echo 'checked';}?>></td>
  </tr>
  <tr>
   <td width="100%" height="8" colspan="2"></td>
  </tr>
   <tr>
   <td>Foolhouse</td>
   <td><input type="checkbox" name="foolhouse" value="1" <? if($wedstrijd_foolhouse == '1'){ echo 'checked';}?>></td>
  </tr>
  <tr>
   <td width="100%" height="8" colspan="2"></td>
  </tr>
  <tr>
   <td width="40%">Naam</td>
   <td width="60%"><input type="text" name="wedstrijd" value="<?php echo $wedstrijd_wedstrijd;?>" size="30"></td>
  </tr>
  <tr>
   <td width="100%" height="8" colspan="2"></td>
  </tr>
  <tr>
   <td width="40%">Link foto's</td>
   <td width="60%"><input type="text" name="foto" value="<?php echo $wedstrijd_foto;?>" size="30"></td>
  </tr>
  <tr>
   <td width="100%" height="8" colspan="2"></td>
  </tr>
  <tr>
   <td>Beschrijving</td>
   <td><textarea name="beschrijving" size="30"><?php echo $wedstrijd_beschrijving;?></textarea></td>
  </tr>
  <tr>
   <td width="100%" height="8" colspan="2"></td>
  </tr>
  <tr>
   <td width="100%" colspan="2"><center><input type="submit" name="update_wedstrijd" value="Update"></center></td>
  </tr>
</table>
</form>

<?php
}
?>
<?php

//prevents caching
header("Expires: Sat, 01 Jan 2000 00:00:00 GMT");
header("Last-Modified: ".gmdate("D, d M Y H:i:s")." GMT");
header("Cache-Control: post-check=0, pre-check=0",false);
session_cache_limiter();
session_start();

require('../config.php');

require('../functions.php');

//this is group name or username of the group or person that you wish to allow access to
// - please be advise that the Administrators Groups has access to all pages.
if (allow_access("Administrators") != "yes")
{
include ('../no_access.html');
exit;
}
$connection = @mysql_connect($server, $dbusername, $dbpassword) or die(mysql_error());
$db = @mysql_select_db($db_name,$connection)or die(mysql_error());

if (isset($_POST['add_wedstrijd'])){
if($_POST['special'] != '1'){
	$_POST['special'] = '0';
}
if($_POST['finale'] != '1'){
	$_POST['finale'] = '0';
}
if($_POST['foolhouse'] != '1'){
	$_POST['foolhouse'] = '0';
}
$sql = "INSERT INTO kalender (locatie, datum, tijd,special,finale,foolhouse, wedstrijd, foto, beschrijving) VALUES ('".$_POST['locatie']."','".$_POST['datum']."','".$_POST['tijd']."','".$_POST['special']."','".$_POST['finale']."','".$_POST['foolhouse']."','".$_POST['wedstrijd']."','".$_POST['foto']."','".$_POST['beschrijving']."');";
$res = mysql_query($sql) or die (mysql_error());

echo 'De wedstrijd is succesvol toegevoegd';

} else {

?>

<form action="<?php echo $PHP_SELF;?>" method="post">

<table border="0" cellpadding="5" cellspacing="0" style="border-collapse: collapse" bordercolor="#111111" width="500">

  <tr>
   <td>Locatie</td>
   <td>
    <select size="1" name="locatie">
    <option value=""></option>

<?php
$sql = "SELECT * FROM locaties ORDER BY naam ASC;";
$resultaat = mysql_query($sql) or die (mysql_error());
while ($land = mysql_fetch_assoc($resultaat)){
echo '    <option value="'.$land['id'].'">'.$land['naam'].'</option>';
}
?>

    </select>
   </td>
  </tr>
 
  <tr>
   <td>Datum (jjjj-mm-dd)</td>
   <td><input type="text" name="datum" size="30"></td>
  </tr>
  
  <tr>
   <td>Tijd (uu:mm:ss)</td>
   <td><input type="text" name="tijd" size="30"></td>
  </tr>
   
  <tr>
   <td>Special Event</td>
   <td><input type="checkbox" name="special" value="1"></td>
  </tr>
 
  <tr>
   <td>Finale</td>
   <td><input type="checkbox" name="finale" value="1"></td>
  </tr>
  
  <tr>
   <td>Foolhouse</td>
   <td><input type="checkbox" name="foolhouse" value="1"></td>
  </tr>
  
  <tr>
   <td>Naam</td>
   <td><input type="text" name="wedstrijd" size="30"></td>
  </tr>
  
  <tr>
   <td>Link foto's</td>
   <td><input type="text" name="foto" size="30"></td>
  </tr>
  
   <tr>
   <td>Beschrijving</td>
   <td><textarea name="beschrijving" size="30"></textarea></td>
  </tr>
  
  <tr>
   <td width="100%" colspan="2"><center><input type="submit" name="add_wedstrijd" value="Toevoegen"></center></td>
  </tr>
</table>
</form>

<?php
}
?>
<?php

//prevents caching
header("Expires: Sat, 01 Jan 2000 00:00:00 GMT");
header("Last-Modified: ".gmdate("D, d M Y H:i:s")." GMT");
header("Cache-Control: post-check=0, pre-check=0",false);
session_cache_limiter();
session_start();

require('../config.php');

require('../functions.php');

//this is group name or username of the group or person that you wish to allow access to
// - please be advise that the Administrators Groups has access to all pages.
if (allow_access("Administrators") != "yes")
{
include ('../no_access.html');
exit;
}
$connection = @mysql_connect($server, $dbusername, $dbpassword) or die(mysql_error());
$db = @mysql_select_db($db_name,$connection)or die(mysql_error());

if (isset($_POST['add_nieuws'])){

$_POST['bericht'] = nl2br($_POST['bericht']);

$sql = "INSERT INTO nieuws (titel, bericht, datum, tijd, auteur) VALUES ('".$_POST['titel']."','".$_POST['bericht']."','".$_POST['datum']."','".time()."','".$_POST['auteur']."');";
$res = mysql_query($sql) or die (mysql_error());

echo 'Het nieuws is succesvol toegevoegd';

} else {

?>

<form action="<?php echo $PHP_SELF;?>" method="post">

<table border="0" cellpadding="0" cellspacing="0" style="border-collapse: collapse" bordercolor="#111111" width="500">
  <tr>
   <td width="156">Titel</td>
   <td width="340" colspan="5"><input type="text" name="titel" size="30"></td>
  </tr>
  <tr>
   <td width="498" height="8" colspan="6"></td>
  </tr>
  <tr>
   <td width="156">Bericht</td>
   <td width="340" colspan="5"><textarea rows="20" name="bericht" cols="40"></textarea></td>
  </tr>
  <tr>
   <td width="498" height="8" colspan="6"></td>
  </tr>
  <tr>
   <td width="156">Datum</td>
   <td width="340" colspan="5"><input type="text" name="datum" value="<?php echo date("Y-m-d");?>" size="30"></td>
  </tr>
  <tr>
   <td width="498" height="8" colspan="6"></td>
  </tr>
  <tr>
   <td width="156">Auteur</td>
   <td width="340" colspan="5"><input type="text" name="auteur" size="30"></td>
  </tr>
  <tr>
   <td width="498" height="8" colspan="6"></td>
  </tr>
  <tr>
   <td width="498" colspan="6"><center><input type="submit" name="add_nieuws" value="Toevoegen"></center></td>
  </tr>
</table>
</form>

<?php
}
?>
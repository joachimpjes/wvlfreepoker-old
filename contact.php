<?
require("top.php");
?>
  <div class="content">
    <div class="grid_12">
      <h2>Over Ons</h2>
<p>WVLFreepoker is opgericht in 2011 door Bart en Frederik en is doorgegroeid van het toemallige freepokerevents. Sindsdien is WVLFreepoker uitgegroeid tot een van de meer toonaangevende poker organisaties in West-Vlaanderen. Ons doel is om gratis poker aan te bieden voor een grote groep spelers met af en toe eens wat evenementjes voor de wat gevorderde spelers. Verspreid over heel Vlaanderen vinden er dagelijks gratis pokertoernooien plaats waar mensen gezellig een kaartje kunnen leggen en vooral een leuke avond kunnen beleven</p>
<p>Als je zin hebt om een keer een live pokertoernooi te spelen, zonder daarvoor veel te moeten uitgeven, dan is WVLFreepoker een geschikte partner voor u wensen. Zowel beginnende als gevorderde spelers zijn bij ons welkom. </p>
<h2>Team</h2>
<p>Het WVLFreepoker team bestaat uit onderstaande 3 vrijwilligers en is dagelijks in de weer om onze spelers de beste beleving te geven. Tijdens onze evenementen staan ze altijd paraat voor vragen en proberen ze een handje mee te helpen indien nodig</p>
<div class="datagrid"><table>
<thead>
<tr>
    <th width="6%">&nbsp</th>
    <th width="13%">Nickname</th>
    <th width="10%">Naam</th>
    <th width="18%">Functie</th>
    </tr>
</thead>
<tbody><tr>
    <td width="6%" align="center"><img src="images/bart.jpg"></td>
    <td>Di Chiappa</td>
    <td>Bart</td>
    <td>General Manager</td>
</tr>
<tr>
  <td width="6%"><img src="images/fredje.jpg"></td>
    <td>Fredje84</td>
    <td>Frederick</td>
    <td>Brand Manager</td>
    </tr>
    <tr>
    <td width="6%"><img src="images/joachim.jpg"></td>
    <td>Joachim</td>
    <td>Joachim</td>
    <td>Head of Poker</td>
</tr></tbody></table></div>
<h2>Contacteer Ons</h2>
<p><a href="mailto:info@wvlfreepoker.be">info@wvlfreepoker.be</a></p>
<p>Wij staan altijd open voor nieuwe locaties die mee in onze WVLfreepoker willen stappen. Wat vragen wij van U ?</p>
<p>
U beschikt over een locatie waar voldoende plaats is om toernooien door te laten gaan<br>
U bent bereid om zelf een finale te laten doorgaan in jouw locatie<br>
Voor de finale worden de prijzen gesponsord door de locaties zelf
</p>
<p>Per deelnemer die de finale weet te halen van jouw locatie, wordt een bedrag betaald. De totale som van alle finalisten van jouw locatie komt in een grote pot terecht samen met de andere locaties. Uit die pot komen de prijzen . Bent u bereid dit te willen doen? </p>
    </div>
  </div>
  <!-- /container -->
  <?
require("onder.php");
?>